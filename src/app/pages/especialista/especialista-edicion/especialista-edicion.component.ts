import { EspecialistaService } from './../../../services/especialista.service';
import { Especialista } from './../../../model/especialista';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-especialista-edicion',
  templateUrl: './especialista-edicion.component.html',
  styleUrls: ['./especialista-edicion.component.css']
})
export class EspecialistaEdicionComponent implements OnInit {
  form:FormGroup;
  id:number;
  especialista:Especialista;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private especialistaService:EspecialistaService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'nombre':new FormControl(''),  
      'rut':new FormControl(''),
      'email':new FormControl(''),
             
    }); 
  }
  initForm(){
    if(this.edicion){
      this.especialistaService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'nombre':new FormControl(data.nombreEspecialista),
          'rut':new FormControl(data.rutEspecialista),
          'email':new FormControl(data.emailEspecialista),
          
        });
      })
        
    }
  }
  ngOnInit() {
    this.especialista=new Especialista();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.especialista.id=this.form.value['id'];
    this.especialista.nombreEspecialista=this.form.value['nombre']; 
    this.especialista.rutEspecialista=this.form.value['rut'];     
    this.especialista.emailEspecialista=this.form.value['email'];     
               
    if(this.edicion){

      this.especialistaService.modificar(this.especialista).subscribe(data=>
      {
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.especialistaCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.especialistaService.registar(this.especialista).subscribe(data=>{
        this.especialistaService.listar().subscribe(especialistas => {this.especialistaService.especialistaCambio.next(especialistas);       
        this.especialistaService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['especialista']);
  }

}
