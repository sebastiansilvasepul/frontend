import { EspecialidadService } from './../../services/especialidad.service';
import { Especialidad } from './../../model/especialidad';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

  dataSource:MatTableDataSource<Especialidad>;
  displayedColumns=['id','descripcion','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private especialidadService:EspecialidadService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.especialidadService.especialidadCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.especialidadService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.especialidadService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }
  eliminar(id:number){
    this.especialidadService.eliminar(id).subscribe(data => {
      this.especialidadService.listar().subscribe(data =>{
        this.especialidadService.especialidadCambio.next(data);
        this.especialidadService.mensajeCambio.next('Se elimino');
      });
    });
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
}
