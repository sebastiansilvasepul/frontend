import { HOST } from './../shared/var.constant';
import { Injectable } from '@angular/core';
import { Examen } from '../model/examen';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {
  examenCambio=new Subject<Examen[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/Examen`;
  constructor(private http:HttpClient) { }
  listar(){    
    return this.http.get<Examen[]>(this.url);

  }
  modificar(examen:Examen){    
    console.log(this.url);
    console.log(examen);
    return this.http.put(this.url,examen);
  }
  registar(examen:Examen){
    
    return this.http.post(this.url,examen);
  }
  listarPorId(id:number){
    
    return this.http.get<Examen>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}