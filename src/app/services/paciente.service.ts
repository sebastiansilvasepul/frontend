import { HOST } from './../shared/var.constant';
import { Injectable } from '@angular/core';
import { Paciente } from '../model/paciente';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  pacienteCambio=new Subject<Paciente[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/Paciente`;
  constructor(private http:HttpClient) { }
  listar(){    
    return this.http.get<Paciente[]>(this.url);

  }
  modificar(paciente:Paciente){    
    console.log(this.url);
    console.log(paciente);
    return this.http.put(this.url,paciente);
  }
  registar(paciente:Paciente){
    
    return this.http.post(this.url,paciente);
  }
  listarPorId(id:number){
    
    return this.http.get<Paciente>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}