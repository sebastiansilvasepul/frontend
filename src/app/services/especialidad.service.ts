import { HttpClient } from '@angular/common/http';
import { HOST } from './../shared/var.constant';
import { Subject } from 'rxjs';
import { Especialidad } from './../model/especialidad';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {
  especialidadCambio=new Subject<Especialidad[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/Especialidadesmedicas`;
  constructor(private http:HttpClient) { }
  listar(){    
    return this.http.get<Especialidad[]>(this.url);

  }
  modificar(especialidad:Especialidad){    
    console.log(this.url);
    console.log(especialidad);
    return this.http.put(this.url,especialidad);
  }
  registar(especialidad:Especialidad){
    
    return this.http.post(this.url,especialidad);
  }
  listarPorId(id:number){
    
    return this.http.get<Especialidad>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}
