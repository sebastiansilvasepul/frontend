import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenComponent } from './pages/examen/examen.component';
import {EspecialistaEdicionComponent} from './pages/especialista/especialista-edicion/especialista-edicion.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { EspecialistaComponent } from './pages/especialista/especialista.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { from } from 'rxjs';

const routes: Routes = [
{ path:"examen",component:ExamenComponent,children:[
  {
    path:'edicion/:id',component:ExamenEdicionComponent
  },
  {
    path:'nuevo',component:ExamenEdicionComponent
  }
]
},
{ path:"especialidad",component:EspecialidadComponent,children:[
  {
    path:'edicion/:id',component:EspecialidadEdicionComponent
  },
  {
    path:'nuevo',component:EspecialidadEdicionComponent
  }
]
},
{ path:"especialista",component:EspecialistaComponent,children:[
  {
    path:'edicion/:id',component:EspecialistaEdicionComponent
  },
  {
    path:'nuevo',component:EspecialistaEdicionComponent
  }
]
},
{ path:"paciente",component:PacienteComponent,children:[
  {
    path:'edicion/:id',component:PacienteEdicionComponent
  },
  {
    path:'nuevo',component:PacienteEdicionComponent
  }
]
}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
